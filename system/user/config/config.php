<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| ExpressionEngine Config Items
|--------------------------------------------------------------------------
|
| The following items are for use with ExpressionEngine.  The rest of
| the config items are for use with CodeIgniter, some of which are not
| observed by ExpressionEngine, e.g. 'permitted_uri_chars'
|
*/

$config['app_version'] = '3.5.11';
$config['cp_url'] = 'http://example.com/admin.php';
$config['doc_url'] = 'https://ellislab.com/expressionengine/user-guide/';
$config['site_label'] = '';$config['multiple_sites_enabled'] = 'n';
$config['session_crypt_key'] = 'ef1dc2bcd201eaasdfe94e51f2bd1e8a97612ee';
$config['encryption_key'] = '8GgDOidKKVltRJasdfo0j7SsZiUUCYd';
$config['cookie_prefix'] = '';
// END EE config items



// ---------------------------------------------------------------
// Numerous CodeIgniter config settings go here. We've removed
// them for breviety in our repository code example
// 
// DO NOT DELETE THE CONFIG SETTINGS THAT WOULD TYPICALLY APPEAR
// HERE. THEY ARE CodeIgniter SETTINGS REQUIRED TO RUN EE PROPERLY
// 
// The config values start with "base_url" and go to "proxy_ips"
// ---------------------------------------------------------------



/**
 * Require the Focus Lab, LLC Master Config file
 */
require $_SERVER['DOCUMENT_ROOT'] . '/../config/config.master.php';

/* End of file config.php */
/* Location: ./system/expressionengine/config/config.php */